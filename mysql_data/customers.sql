-- phpMyAdmin SQL Dump
-- version 3.4.7.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 11, 2012 at 01:22 AM
-- Server version: 5.1.56
-- PHP Version: 5.2.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `danielle_youtube`
--

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE IF NOT EXISTS `customers` (
  `id` int(11) NOT NULL,
  `name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `zip` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `address`, `city`, `state`, `zip`) VALUES
(1, 'Bucky Roberts', '32 Hungerford Ave', 'Adams', 'NY', 13605),
(2, 'Noah Parker', '342 Tunafish Lane', 'Raleigh', 'NC', 27606),
(3, 'Kelsey Burger', '43 Crab Orchard Dr', 'Oakland', 'CA', 90210),
(4, 'Corey Smith', '423 Wisteria Lane', 'Simmersville', 'AK', 54112),
(5, 'Harry Potter', '673 Greenwich Ave', 'Newark', 'NJ', 80432),
(6, 'Henry Jackson', '2134 Grant St', 'Gary', 'IN', 47404),
(7, 'Cynthia Alvarez', '1568 Greenfield Ave', 'Augusta', 'GA', 30568),
(8, 'Teresa Smith', '8725 Black St', 'Philadelphia', 'PA', 19603),
(9, 'Gary Foster', '3752 Wittfield Blvd', 'Indianapolis', 'IN', 46219),
(10, 'Sherry Gibbons', '5517 Oak St', 'Phoenix', 'AZ', 85072);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;